import Headroom from 'headroom.js/dist/headroom'

export default function header() {
  const header = document.querySelector('.header')
  const headroom = new Headroom(header, {
    'offset': 205,
    'tolerance': 5,
    'classes': {
      'initial': 'animated',
      'pinned': 'slideDown',
      'unpinned': 'slideUp'
    }
  })

  headroom.init()
}
