import smoothScroll from 'smooth-scroll'
export default function account () {
    var buttonSearch = document.querySelector('.headerLogin')
    var closeAccount = document.querySelector('.accountClose')
    buttonSearch.addEventListener('click', function () {
        smoothScroll.animateScroll( 0 )
        document.querySelector('body').classList.add('accountShow')
        document.querySelector('.contentFade').addEventListener('click', function () {
            document.querySelector('body').classList.remove('accountShow')
        })
    })

    closeAccount.addEventListener('click', function () {
        document.querySelector('body').classList.remove('accountShow')
    })

}
