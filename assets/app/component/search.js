export default function search () {
    var buttonSearch = document.querySelector('.headerSearch')
    var closeSidebar = document.querySelector('.sidebarClose')
    buttonSearch.addEventListener('click', function () {
        document.querySelector('body').classList.add('searchShow')
        document.querySelector('.contentFade').addEventListener('click', function () {
            document.querySelector('body').classList.remove('searchShow')
        })
    })
}
