import Swiper from 'swiper/dist/js/swiper.js'


export default function product () {
  var productSwiper = new Swiper('.productSlider', {
    slidesPerView: 1,
    spaceBetween: 0,
    direction: 'horizontal',
    loop: false
  })
  Array.from(document.querySelectorAll('.productThumbItem')).forEach((el, i) => {
    el.addEventListener('click', () => {
      productSwiper.slideTo(i)
    })
  })
}
