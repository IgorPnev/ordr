import Headroom from 'headroom.js/dist/headroom'

export default function footer () {
  const header = document.querySelector('.footer')
  const headroom = new Headroom(header, {
    'offset': -65,
    'tolerance': 5,
    'classes': {
      'initial': 'footerAnimate',
      //'pinned': 'slideDown',
      'notTop' : 'footerNotTop',
      'bottom' : 'slideUp'
    }
  })
  if (window.matchMedia('(min-width: 600px)').matches)
    headroom.init()

  const footerStore = document.querySelector('.js-store')
  footerStore.addEventListener('click', () => {
    document.querySelector('.js-storeContent').classList.toggle('footerMenuItemPopupShow')
  })
  const footerFollow = document.querySelector('.js-follow')
  footerFollow.addEventListener('click', () => {
    document.querySelector('.js-followContent').classList.toggle('footerMenuItemPopupShow')
  })
}
