import Ps from 'perfect-scrollbar'

export default function sidebar () {
    var buttonCart = document.querySelector('.headerCart')
    var closeSidebar = document.querySelector('.sidebarClose')
    buttonCart.addEventListener('click', function () {
        document.querySelector('body').classList.add('sidebarShow')
    })
    closeSidebar.addEventListener('click', function () {
        document.querySelector('body').classList.remove('sidebarShow')
    })

    var scrollContainer = document.querySelector('.sidebarScrollContainer')
    Ps.initialize(scrollContainer, {
      minScrollbarLength: 16,
      maxScrollbarLength: 16
    })
}
