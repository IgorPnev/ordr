import Swiper from 'swiper/dist/js/swiper.js'
export default function mainSlider () {
  var swiper = new Swiper('.mainSlider', {
    slidesPerView: 1,
    spaceBetween: 0,
    direction: 'horizontal',
    prevButton: '.mainSliderPrev',
    nextButton: '.mainSliderNext',
    loop: true,
    pagination: '.swiper-pagination',
    paginationClickable: true
  })
}
