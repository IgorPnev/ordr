import mainSlider from './component/mainSlider'
import product from './component/product'
import sidebar from './component/sidebar'
import header from './component/header'
import footer from './component/footer'
import search from './component/search'
import account from './component/account'
import mobile from './component/mobile'

document.addEventListener('DOMContentLoaded', function () {
  mainSlider()
  product()
  sidebar()
  header()
  footer()
  search()
  mobile()
  account()
})
module.exports = {
  updateSidebar: function () {
    sidebar()
  }
}
