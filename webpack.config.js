var path = require('path')
var webpack = require('webpack')


    module.exports = {
        watch: true,
        entry: {
            preload: './assets/app/preload.js',
            app: './assets/app/app.js'
        },
        output: {
            path: __dirname + '/public/js',
            filename: '[name].js',
            libraryTarget: 'var',
            library: '[name]'
        },
        module: {
            loaders: [ {
              test: /\.js?$/,
              exclude: /(node_modules)/,
              loader: 'babel',
              query: {
                // cacheDirectory: true,
                // optional: ['runtime'],
                // stage: 0
                presets: [ 'es2015' ]
              }
            } ]
        },
        devtool: 'source-map',

        plugins: [
        ]

}
