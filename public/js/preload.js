var preload =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(32);
	__webpack_require__(33).init();
	document.addEventListener('DOMContentLoaded', function () {});

/***/ },

/***/ 32:
/***/ function(module, exports, __webpack_require__) {

	var require;var require;!function(e){if(true)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var t;t="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,t.flexibility=e()}}(function(){return function e(t,r,l){function n(f,i){if(!r[f]){if(!t[f]){var s="function"==typeof require&&require;if(!i&&s)return require(f,!0);if(o)return o(f,!0);var a=new Error("Cannot find module '"+f+"'");throw a.code="MODULE_NOT_FOUND",a}var c=r[f]={exports:{}};t[f][0].call(c.exports,function(e){var r=t[f][1][e];return n(r?r:e)},c,c.exports,e,t,r,l)}return r[f].exports}for(var o="function"==typeof require&&require,f=0;f<l.length;f++)n(l[f]);return n}({1:[function(e,t,r){t.exports=function(e){var t,r,l,n=-1;if(e.lines.length>1&&"flex-start"===e.style.alignContent)for(t=0;l=e.lines[++n];)l.crossStart=t,t+=l.cross;else if(e.lines.length>1&&"flex-end"===e.style.alignContent)for(t=e.flexStyle.crossSpace;l=e.lines[++n];)l.crossStart=t,t+=l.cross;else if(e.lines.length>1&&"center"===e.style.alignContent)for(t=e.flexStyle.crossSpace/2;l=e.lines[++n];)l.crossStart=t,t+=l.cross;else if(e.lines.length>1&&"space-between"===e.style.alignContent)for(r=e.flexStyle.crossSpace/(e.lines.length-1),t=0;l=e.lines[++n];)l.crossStart=t,t+=l.cross+r;else if(e.lines.length>1&&"space-around"===e.style.alignContent)for(r=2*e.flexStyle.crossSpace/(2*e.lines.length),t=r/2;l=e.lines[++n];)l.crossStart=t,t+=l.cross+r;else for(r=e.flexStyle.crossSpace/e.lines.length,t=e.flexStyle.crossInnerBefore;l=e.lines[++n];)l.crossStart=t,l.cross+=r,t+=l.cross}},{}],2:[function(e,t,r){t.exports=function(e){for(var t,r=-1;line=e.lines[++r];)for(t=-1;child=line.children[++t];){var l=child.style.alignSelf;"auto"===l&&(l=e.style.alignItems),"flex-start"===l?child.flexStyle.crossStart=line.crossStart:"flex-end"===l?child.flexStyle.crossStart=line.crossStart+line.cross-child.flexStyle.crossOuter:"center"===l?child.flexStyle.crossStart=line.crossStart+(line.cross-child.flexStyle.crossOuter)/2:(child.flexStyle.crossStart=line.crossStart,child.flexStyle.crossOuter=line.cross,child.flexStyle.cross=child.flexStyle.crossOuter-child.flexStyle.crossBefore-child.flexStyle.crossAfter)}}},{}],3:[function(e,t,r){t.exports=function l(e,l){var t="row"===l||"row-reverse"===l,r=e.mainAxis;if(r){var n=t&&"inline"===r||!t&&"block"===r;n||(e.flexStyle={main:e.flexStyle.cross,cross:e.flexStyle.main,mainOffset:e.flexStyle.crossOffset,crossOffset:e.flexStyle.mainOffset,mainBefore:e.flexStyle.crossBefore,mainAfter:e.flexStyle.crossAfter,crossBefore:e.flexStyle.mainBefore,crossAfter:e.flexStyle.mainAfter,mainInnerBefore:e.flexStyle.crossInnerBefore,mainInnerAfter:e.flexStyle.crossInnerAfter,crossInnerBefore:e.flexStyle.mainInnerBefore,crossInnerAfter:e.flexStyle.mainInnerAfter,mainBorderBefore:e.flexStyle.crossBorderBefore,mainBorderAfter:e.flexStyle.crossBorderAfter,crossBorderBefore:e.flexStyle.mainBorderBefore,crossBorderAfter:e.flexStyle.mainBorderAfter})}else t?e.flexStyle={main:e.style.width,cross:e.style.height,mainOffset:e.style.offsetWidth,crossOffset:e.style.offsetHeight,mainBefore:e.style.marginLeft,mainAfter:e.style.marginRight,crossBefore:e.style.marginTop,crossAfter:e.style.marginBottom,mainInnerBefore:e.style.paddingLeft,mainInnerAfter:e.style.paddingRight,crossInnerBefore:e.style.paddingTop,crossInnerAfter:e.style.paddingBottom,mainBorderBefore:e.style.borderLeftWidth,mainBorderAfter:e.style.borderRightWidth,crossBorderBefore:e.style.borderTopWidth,crossBorderAfter:e.style.borderBottomWidth}:e.flexStyle={main:e.style.height,cross:e.style.width,mainOffset:e.style.offsetHeight,crossOffset:e.style.offsetWidth,mainBefore:e.style.marginTop,mainAfter:e.style.marginBottom,crossBefore:e.style.marginLeft,crossAfter:e.style.marginRight,mainInnerBefore:e.style.paddingTop,mainInnerAfter:e.style.paddingBottom,crossInnerBefore:e.style.paddingLeft,crossInnerAfter:e.style.paddingRight,mainBorderBefore:e.style.borderTopWidth,mainBorderAfter:e.style.borderBottomWidth,crossBorderBefore:e.style.borderLeftWidth,crossBorderAfter:e.style.borderRightWidth},"content-box"===e.style.boxSizing&&("number"==typeof e.flexStyle.main&&(e.flexStyle.main+=e.flexStyle.mainInnerBefore+e.flexStyle.mainInnerAfter+e.flexStyle.mainBorderBefore+e.flexStyle.mainBorderAfter),"number"==typeof e.flexStyle.cross&&(e.flexStyle.cross+=e.flexStyle.crossInnerBefore+e.flexStyle.crossInnerAfter+e.flexStyle.crossBorderBefore+e.flexStyle.crossBorderAfter));e.mainAxis=t?"inline":"block",e.crossAxis=t?"block":"inline","number"==typeof e.style.flexBasis&&(e.flexStyle.main=e.style.flexBasis+e.flexStyle.mainInnerBefore+e.flexStyle.mainInnerAfter+e.flexStyle.mainBorderBefore+e.flexStyle.mainBorderAfter),e.flexStyle.mainOuter=e.flexStyle.main,e.flexStyle.crossOuter=e.flexStyle.cross,"auto"===e.flexStyle.mainOuter&&(e.flexStyle.mainOuter=e.flexStyle.mainOffset),"auto"===e.flexStyle.crossOuter&&(e.flexStyle.crossOuter=e.flexStyle.crossOffset),"number"==typeof e.flexStyle.mainBefore&&(e.flexStyle.mainOuter+=e.flexStyle.mainBefore),"number"==typeof e.flexStyle.mainAfter&&(e.flexStyle.mainOuter+=e.flexStyle.mainAfter),"number"==typeof e.flexStyle.crossBefore&&(e.flexStyle.crossOuter+=e.flexStyle.crossBefore),"number"==typeof e.flexStyle.crossAfter&&(e.flexStyle.crossOuter+=e.flexStyle.crossAfter)}},{}],4:[function(e,t,r){var l=e("../reduce");t.exports=function(e){if(e.mainSpace>0){var t=l(e.children,function(e,t){return e+parseFloat(t.style.flexGrow)},0);t>0&&(e.main=l(e.children,function(r,l){return"auto"===l.flexStyle.main?l.flexStyle.main=l.flexStyle.mainOffset+parseFloat(l.style.flexGrow)/t*e.mainSpace:l.flexStyle.main+=parseFloat(l.style.flexGrow)/t*e.mainSpace,l.flexStyle.mainOuter=l.flexStyle.main+l.flexStyle.mainBefore+l.flexStyle.mainAfter,r+l.flexStyle.mainOuter},0),e.mainSpace=0)}}},{"../reduce":12}],5:[function(e,t,r){var l=e("../reduce");t.exports=function(e){if(e.mainSpace<0){var t=l(e.children,function(e,t){return e+parseFloat(t.style.flexShrink)},0);t>0&&(e.main=l(e.children,function(r,l){return l.flexStyle.main+=parseFloat(l.style.flexShrink)/t*e.mainSpace,l.flexStyle.mainOuter=l.flexStyle.main+l.flexStyle.mainBefore+l.flexStyle.mainAfter,r+l.flexStyle.mainOuter},0),e.mainSpace=0)}}},{"../reduce":12}],6:[function(e,t,r){var l=e("../reduce");t.exports=function(e){var t;e.lines=[t={main:0,cross:0,children:[]}];for(var r,n=-1;r=e.children[++n];)"nowrap"===e.style.flexWrap||0===t.children.length||"auto"===e.flexStyle.main||e.flexStyle.main-e.flexStyle.mainInnerBefore-e.flexStyle.mainInnerAfter-e.flexStyle.mainBorderBefore-e.flexStyle.mainBorderAfter>=t.main+r.flexStyle.mainOuter?(t.main+=r.flexStyle.mainOuter,t.cross=Math.max(t.cross,r.flexStyle.crossOuter)):e.lines.push(t={main:r.flexStyle.mainOuter,cross:r.flexStyle.crossOuter,children:[]}),t.children.push(r);e.flexStyle.mainLines=l(e.lines,function(e,t){return Math.max(e,t.main)},0),e.flexStyle.crossLines=l(e.lines,function(e,t){return e+t.cross},0),"auto"===e.flexStyle.main&&(e.flexStyle.main=Math.max(e.flexStyle.mainOffset,e.flexStyle.mainLines+e.flexStyle.mainInnerBefore+e.flexStyle.mainInnerAfter+e.flexStyle.mainBorderBefore+e.flexStyle.mainBorderAfter)),"auto"===e.flexStyle.cross&&(e.flexStyle.cross=Math.max(e.flexStyle.crossOffset,e.flexStyle.crossLines+e.flexStyle.crossInnerBefore+e.flexStyle.crossInnerAfter+e.flexStyle.crossBorderBefore+e.flexStyle.crossBorderAfter)),e.flexStyle.crossSpace=e.flexStyle.cross-e.flexStyle.crossInnerBefore-e.flexStyle.crossInnerAfter-e.flexStyle.crossBorderBefore-e.flexStyle.crossBorderAfter-e.flexStyle.crossLines,e.flexStyle.mainOuter=e.flexStyle.main+e.flexStyle.mainBefore+e.flexStyle.mainAfter,e.flexStyle.crossOuter=e.flexStyle.cross+e.flexStyle.crossBefore+e.flexStyle.crossAfter}},{"../reduce":12}],7:[function(e,t,r){function l(t){for(var r,l=-1;r=t.children[++l];)e("./flex-direction")(r,t.style.flexDirection);e("./flex-direction")(t,t.style.flexDirection),e("./order")(t),e("./flexbox-lines")(t),e("./align-content")(t),l=-1;for(var n;n=t.lines[++l];)n.mainSpace=t.flexStyle.main-t.flexStyle.mainInnerBefore-t.flexStyle.mainInnerAfter-t.flexStyle.mainBorderBefore-t.flexStyle.mainBorderAfter-n.main,e("./flex-grow")(n),e("./flex-shrink")(n),e("./margin-main")(n),e("./margin-cross")(n),e("./justify-content")(n,t.style.justifyContent,t);e("./align-items")(t)}t.exports=l},{"./align-content":1,"./align-items":2,"./flex-direction":3,"./flex-grow":4,"./flex-shrink":5,"./flexbox-lines":6,"./justify-content":8,"./margin-cross":9,"./margin-main":10,"./order":11}],8:[function(e,t,r){t.exports=function(e,t,r){var l,n,o,f=r.flexStyle.mainInnerBefore,i=-1;if("flex-end"===t)for(l=e.mainSpace,l+=f;o=e.children[++i];)o.flexStyle.mainStart=l,l+=o.flexStyle.mainOuter;else if("center"===t)for(l=e.mainSpace/2,l+=f;o=e.children[++i];)o.flexStyle.mainStart=l,l+=o.flexStyle.mainOuter;else if("space-between"===t)for(n=e.mainSpace/(e.children.length-1),l=0,l+=f;o=e.children[++i];)o.flexStyle.mainStart=l,l+=o.flexStyle.mainOuter+n;else if("space-around"===t)for(n=2*e.mainSpace/(2*e.children.length),l=n/2,l+=f;o=e.children[++i];)o.flexStyle.mainStart=l,l+=o.flexStyle.mainOuter+n;else for(l=0,l+=f;o=e.children[++i];)o.flexStyle.mainStart=l,l+=o.flexStyle.mainOuter}},{}],9:[function(e,t,r){t.exports=function(e){for(var t,r=-1;t=e.children[++r];){var l=0;"auto"===t.flexStyle.crossBefore&&++l,"auto"===t.flexStyle.crossAfter&&++l;var n=e.cross-t.flexStyle.crossOuter;"auto"===t.flexStyle.crossBefore&&(t.flexStyle.crossBefore=n/l),"auto"===t.flexStyle.crossAfter&&(t.flexStyle.crossAfter=n/l),"auto"===t.flexStyle.cross?t.flexStyle.crossOuter=t.flexStyle.crossOffset+t.flexStyle.crossBefore+t.flexStyle.crossAfter:t.flexStyle.crossOuter=t.flexStyle.cross+t.flexStyle.crossBefore+t.flexStyle.crossAfter}}},{}],10:[function(e,t,r){t.exports=function(e){for(var t,r=0,l=-1;t=e.children[++l];)"auto"===t.flexStyle.mainBefore&&++r,"auto"===t.flexStyle.mainAfter&&++r;if(r>0){for(l=-1;t=e.children[++l];)"auto"===t.flexStyle.mainBefore&&(t.flexStyle.mainBefore=e.mainSpace/r),"auto"===t.flexStyle.mainAfter&&(t.flexStyle.mainAfter=e.mainSpace/r),"auto"===t.flexStyle.main?t.flexStyle.mainOuter=t.flexStyle.mainOffset+t.flexStyle.mainBefore+t.flexStyle.mainAfter:t.flexStyle.mainOuter=t.flexStyle.main+t.flexStyle.mainBefore+t.flexStyle.mainAfter;e.mainSpace=0}}},{}],11:[function(e,t,r){var l=/^(column|row)-reverse$/;t.exports=function(e){e.children.sort(function(e,t){return e.style.order-t.style.order||e.index-t.index}),l.test(e.style.flexDirection)&&e.children.reverse()}},{}],12:[function(e,t,r){function l(e,t,r){for(var l=e.length,n=-1;++n<l;)n in e&&(r=t(r,e[n],n));return r}t.exports=l},{}],13:[function(e,t,r){function l(e){i(f(e))}var n=e("./read"),o=e("./write"),f=e("./readAll"),i=e("./writeAll");t.exports=l,t.exports.read=n,t.exports.write=o,t.exports.readAll=f,t.exports.writeAll=i},{"./read":15,"./readAll":16,"./write":17,"./writeAll":18}],14:[function(e,t,r){function l(e,t,r){var l=e[t],f=String(l).match(o);if(!f){var a=t.match(s);if(a){var c=e["border"+a[1]+"Style"];return"none"===c?0:i[l]||0}return l}var y=f[1],x=f[2];return"px"===x?1*y:"cm"===x?.3937*y*96:"in"===x?96*y:"mm"===x?.3937*y*96/10:"pc"===x?12*y*96/72:"pt"===x?96*y/72:"rem"===x?16*y:n(l,r)}function n(e,t){f.style.cssText="border:none!important;clip:rect(0 0 0 0)!important;display:block!important;font-size:1em!important;height:0!important;margin:0!important;padding:0!important;position:relative!important;width:"+e+"!important",t.parentNode.insertBefore(f,t.nextSibling);var r=f.offsetWidth;return t.parentNode.removeChild(f),r}t.exports=l;var o=/^([-+]?\d*\.?\d+)(%|[a-z]+)$/,f=document.createElement("div"),i={medium:4,none:0,thick:6,thin:2},s=/^border(Bottom|Left|Right|Top)Width$/},{}],15:[function(e,t,r){function l(e){var t={alignContent:"stretch",alignItems:"stretch",alignSelf:"auto",borderBottomStyle:"none",borderBottomWidth:0,borderLeftStyle:"none",borderLeftWidth:0,borderRightStyle:"none",borderRightWidth:0,borderTopStyle:"none",borderTopWidth:0,boxSizing:"content-box",display:"inline",flexBasis:"auto",flexDirection:"row",flexGrow:0,flexShrink:1,flexWrap:"nowrap",justifyContent:"flex-start",height:"auto",marginTop:0,marginRight:0,marginLeft:0,marginBottom:0,paddingTop:0,paddingRight:0,paddingLeft:0,paddingBottom:0,maxHeight:"none",maxWidth:"none",minHeight:0,minWidth:0,order:0,position:"static",width:"auto"},r=e instanceof Element;if(r){var l=e.hasAttribute("data-style"),i=l?e.getAttribute("data-style"):e.getAttribute("style")||"";l||e.setAttribute("data-style",i);var s=window.getComputedStyle&&getComputedStyle(e)||{};f(t,s);var c=e.currentStyle||{};n(t,c),o(t,i);for(var y in t)t[y]=a(t,y,e);var x=e.getBoundingClientRect();t.offsetHeight=x.height||e.offsetHeight,t.offsetWidth=x.width||e.offsetWidth}var S={element:e,style:t};return S}function n(e,t){for(var r in e){var l=r in t;if(l)e[r]=t[r];else{var n=r.replace(/[A-Z]/g,"-$&").toLowerCase(),o=n in t;o&&(e[r]=t[n])}}var f="-js-display"in t;f&&(e.display=t["-js-display"])}function o(e,t){for(var r;r=i.exec(t);){var l=r[1].toLowerCase().replace(/-[a-z]/g,function(e){return e.slice(1).toUpperCase()});e[l]=r[2]}}function f(e,t){for(var r in e){var l=r in t;l&&!s.test(r)&&(e[r]=t[r])}}t.exports=l;var i=/([^\s:;]+)\s*:\s*([^;]+?)\s*(;|$)/g,s=/^(alignSelf|height|width)$/,a=e("./getComputedLength")},{"./getComputedLength":14}],16:[function(e,t,r){function l(e){var t=[];return n(e,t),t}function n(e,t){for(var r,l=o(e),i=[],s=-1;r=e.childNodes[++s];){var a=3===r.nodeType&&!/^\s*$/.test(r.nodeValue);if(l&&a){var c=r;r=e.insertBefore(document.createElement("flex-item"),c),r.appendChild(c)}var y=r instanceof Element;if(y){var x=n(r,t);if(l){var S=r.style;S.display="inline-block",S.position="absolute",x.style=f(r).style,i.push(x)}}}var m={element:e,children:i};return l&&(m.style=f(e).style,t.push(m)),m}function o(e){var t=e instanceof Element,r=t&&e.getAttribute("data-style"),l=t&&e.currentStyle&&e.currentStyle["-js-display"],n=i.test(r)||s.test(l);return n}t.exports=l;var f=e("../read"),i=/(^|;)\s*display\s*:\s*(inline-)?flex\s*(;|$)/i,s=/^(inline-)?flex$/i},{"../read":15}],17:[function(e,t,r){function l(e){o(e);var t=e.element.style,r="inline"===e.mainAxis?["main","cross"]:["cross","main"];t.boxSizing="content-box",t.display="block",t.position="relative",t.width=n(e.flexStyle[r[0]]-e.flexStyle[r[0]+"InnerBefore"]-e.flexStyle[r[0]+"InnerAfter"]-e.flexStyle[r[0]+"BorderBefore"]-e.flexStyle[r[0]+"BorderAfter"]),t.height=n(e.flexStyle[r[1]]-e.flexStyle[r[1]+"InnerBefore"]-e.flexStyle[r[1]+"InnerAfter"]-e.flexStyle[r[1]+"BorderBefore"]-e.flexStyle[r[1]+"BorderAfter"]);for(var l,f=-1;l=e.children[++f];){var i=l.element.style,s="inline"===l.mainAxis?["main","cross"]:["cross","main"];i.boxSizing="content-box",i.display="block",i.position="absolute","auto"!==l.flexStyle[s[0]]&&(i.width=n(l.flexStyle[s[0]]-l.flexStyle[s[0]+"InnerBefore"]-l.flexStyle[s[0]+"InnerAfter"]-l.flexStyle[s[0]+"BorderBefore"]-l.flexStyle[s[0]+"BorderAfter"])),"auto"!==l.flexStyle[s[1]]&&(i.height=n(l.flexStyle[s[1]]-l.flexStyle[s[1]+"InnerBefore"]-l.flexStyle[s[1]+"InnerAfter"]-l.flexStyle[s[1]+"BorderBefore"]-l.flexStyle[s[1]+"BorderAfter"])),i.top=n(l.flexStyle[s[1]+"Start"]),i.left=n(l.flexStyle[s[0]+"Start"]),i.marginTop=n(l.flexStyle[s[1]+"Before"]),i.marginRight=n(l.flexStyle[s[0]+"After"]),i.marginBottom=n(l.flexStyle[s[1]+"After"]),i.marginLeft=n(l.flexStyle[s[0]+"Before"])}}function n(e){return"string"==typeof e?e:Math.max(e,0)+"px"}t.exports=l;var o=e("../flexbox")},{"../flexbox":7}],18:[function(e,t,r){function l(e){for(var t,r=-1;t=e[++r];)n(t)}t.exports=l;var n=e("../write")},{"../write":17}]},{},[13])(13)});

/***/ },

/***/ 33:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	 * viewport-units-buggyfill v0.6.0
	 * @web: https://github.com/rodneyrehm/viewport-units-buggyfill/
	 * @author: Rodney Rehm - http://rodneyrehm.de/en/
	 */
	
	(function (root, factory) {
	  'use strict';
	  if (true) {
	    // AMD. Register as an anonymous module.
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (typeof exports === 'object') {
	    // Node. Does not work with strict CommonJS, but
	    // only CommonJS-like enviroments that support module.exports,
	    // like Node.
	    module.exports = factory();
	  } else {
	    // Browser globals (root is window)
	    root.viewportUnitsBuggyfill = factory();
	  }
	}(this, function () {
	  'use strict';
	  /*global document, window, navigator, location, XMLHttpRequest, XDomainRequest, CustomEvent*/
	
	  var initialized = false;
	  var options;
	  var userAgent = window.navigator.userAgent;
	  var viewportUnitExpression = /([+-]?[0-9.]+)(vh|vw|vmin|vmax)/g;
	  var forEach = [].forEach;
	  var dimensions;
	  var declarations;
	  var styleNode;
	  var isBuggyIE = /MSIE [0-9]\./i.test(userAgent);
	  var isOldIE = /MSIE [0-8]\./i.test(userAgent);
	  var isOperaMini = userAgent.indexOf('Opera Mini') > -1;
	
	  var isMobileSafari = /(iPhone|iPod|iPad).+AppleWebKit/i.test(userAgent) && (function() {
	    // Regexp for iOS-version tested against the following userAgent strings:
	    // Example WebView UserAgents:
	    // * iOS Chrome on iOS8: "Mozilla/5.0 (iPad; CPU OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) CriOS/39.0.2171.50 Mobile/12B410 Safari/600.1.4"
	    // * iOS Facebook on iOS7: "Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D201 [FBAN/FBIOS;FBAV/12.1.0.24.20; FBBV/3214247; FBDV/iPhone6,1;FBMD/iPhone; FBSN/iPhone OS;FBSV/7.1.1; FBSS/2; FBCR/AT&T;FBID/phone;FBLC/en_US;FBOP/5]"
	    // Example Safari UserAgents:
	    // * Safari iOS8: "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4"
	    // * Safari iOS7: "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A4449d Safari/9537.53"
	    var iOSversion = userAgent.match(/OS (\d)/);
	    // viewport units work fine in mobile Safari and webView on iOS 8+
	    return iOSversion && iOSversion.length>1 && parseInt(iOSversion[1]) < 10;
	  })();
	
	  var isBadStockAndroid = (function() {
	    // Android stock browser test derived from
	    // http://stackoverflow.com/questions/24926221/distinguish-android-chrome-from-stock-browser-stock-browsers-user-agent-contai
	    var isAndroid = userAgent.indexOf(' Android ') > -1;
	    if (!isAndroid) {
	      return false;
	    }
	
	    var isStockAndroid = userAgent.indexOf('Version/') > -1;
	    if (!isStockAndroid) {
	      return false;
	    }
	
	    var versionNumber = parseFloat((userAgent.match('Android ([0-9.]+)') || [])[1]);
	    // anything below 4.4 uses WebKit without *any* viewport support,
	    // 4.4 has issues with viewport units within calc()
	    return versionNumber <= 4.4;
	  })();
	
	  // added check for IE10, IE11 and Edge < 20, since it *still* doesn't understand vmax
	  // http://caniuse.com/#feat=viewport-units
	  if (!isBuggyIE) {
	    isBuggyIE = !!navigator.userAgent.match(/Trident.*rv[ :]*1[01]\.| Edge\/1\d\./);
	  }
	
	  // Polyfill for creating CustomEvents on IE9/10/11
	  // from https://github.com/krambuhl/custom-event-polyfill
	  try {
	    new CustomEvent('test');
	  } catch(e) {
	    var CustomEvent = function(event, params) {
	      var evt;
	      params = params || {
	        bubbles: false,
	        cancelable: false,
	        detail: undefined
	      };
	
	      evt = document.createEvent('CustomEvent');
	      evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
	      return evt;
	    };
	    CustomEvent.prototype = window.Event.prototype;
	    window.CustomEvent = CustomEvent; // expose definition to window
	  }
	
	  function debounce(func, wait) {
	    var timeout;
	    return function() {
	      var context = this;
	      var args = arguments;
	      var callback = function() {
	        func.apply(context, args);
	      };
	
	      clearTimeout(timeout);
	      timeout = setTimeout(callback, wait);
	    };
	  }
	
	  // from http://stackoverflow.com/questions/326069/how-to-identify-if-a-webpage-is-being-loaded-inside-an-iframe-or-directly-into-t
	  function inIframe() {
	    try {
	      return window.self !== window.top;
	    } catch (e) {
	      return true;
	    }
	  }
	
	  function initialize(initOptions) {
	    if (initialized) {
	      return;
	    }
	
	    if (initOptions === true) {
	      initOptions = {
	        force: true
	      };
	    }
	
	    options = initOptions || {};
	    options.isMobileSafari = isMobileSafari;
	    options.isBadStockAndroid = isBadStockAndroid;
	
	    if (options.ignoreVmax && !options.force && !isOldIE) {
	      // modern IE (10 and up) do not support vmin/vmax,
	      // but chances are this unit is not even used, so
	      // allow overwriting the "hacktivation"
	      // https://github.com/rodneyrehm/viewport-units-buggyfill/issues/56
	      isBuggyIE = false;
	    }
	
	    if (isOldIE || (!options.force && !isMobileSafari && !isBuggyIE && !isBadStockAndroid && !isOperaMini && (!options.hacks || !options.hacks.required(options)))) {
	      // this buggyfill only applies to mobile safari, IE9-10 and the Stock Android Browser.
	      if (window.console && isOldIE) {
	        console.info('viewport-units-buggyfill requires a proper CSSOM and basic viewport unit support, which are not available in IE8 and below');
	      }
	
	      return {
	        init: function () {}
	      };
	    }
	
	    // fire a custom event that buggyfill was initialize
	    window.dispatchEvent(new CustomEvent('viewport-units-buggyfill-init'));
	
	    options.hacks && options.hacks.initialize(options);
	
	    initialized = true;
	    styleNode = document.createElement('style');
	    styleNode.id = 'patched-viewport';
	    document.head.appendChild(styleNode);
	
	    // Issue #6: Cross Origin Stylesheets are not accessible through CSSOM,
	    // therefore download and inject them as <style> to circumvent SOP.
	    importCrossOriginLinks(function() {
	      var _refresh = debounce(refresh, options.refreshDebounceWait || 100);
	      // doing a full refresh rather than updateStyles because an orientationchange
	      // could activate different stylesheets
	      window.addEventListener('orientationchange', _refresh, true);
	      // orientationchange might have happened while in a different window
	      window.addEventListener('pageshow', _refresh, true);
	
	      if (options.force || isBuggyIE || inIframe()) {
	        window.addEventListener('resize', _refresh, true);
	        options._listeningToResize = true;
	      }
	
	      options.hacks && options.hacks.initializeEvents(options, refresh, _refresh);
	
	      refresh();
	    });
	  }
	
	  function updateStyles() {
	    styleNode.textContent = getReplacedViewportUnits();
	    // move to the end in case inline <style>s were added dynamically
	    styleNode.parentNode.appendChild(styleNode);
	    // fire a custom event that styles were updated
	    window.dispatchEvent(new CustomEvent('viewport-units-buggyfill-style'));
	  }
	
	  function refresh() {
	    if (!initialized) {
	      return;
	    }
	
	    findProperties();
	
	    // iOS Safari will report window.innerWidth and .innerHeight as 0 unless a timeout is used here.
	    // TODO: figure out WHY innerWidth === 0
	    setTimeout(function() {
	      updateStyles();
	    }, 1);
	  }
	  
	  // http://stackoverflow.com/a/23613052
	  function processStylesheet(ss) {
	    // cssRules respects same-origin policy, as per
	    // https://code.google.com/p/chromium/issues/detail?id=49001#c10.
	    try {
	      if (!ss.cssRules) { return; }
	    } catch(e) {
	      if (e.name !== 'SecurityError') { throw e; }
	      return;
	    }
	    // ss.cssRules is available, so proceed with desired operations.
	    var rules = [];
	    for (var i = 0; i < ss.cssRules.length; i++) {
	      var rule = ss.cssRules[i];
	      rules.push(rule);
	    }
	    return rules;
	  }
	
	  function findProperties() {
	    declarations = [];
	    forEach.call(document.styleSheets, function(sheet) {
	      var cssRules = processStylesheet(sheet);
	
	      if (!cssRules || sheet.ownerNode.id === 'patched-viewport' || sheet.ownerNode.getAttribute('data-viewport-units-buggyfill') === 'ignore') {
	        // skip entire sheet because no rules are present, it's supposed to be ignored or it's the target-element of the buggyfill
	        return;
	      }
	
	      if (sheet.media && sheet.media.mediaText && window.matchMedia && !window.matchMedia(sheet.media.mediaText).matches) {
	        // skip entire sheet because media attribute doesn't match
	        return;
	      }
	
	      forEach.call(cssRules, findDeclarations);
	    });
	
	    return declarations;
	  }
	
	  function findDeclarations(rule) {
	    if (rule.type === 7) {
	      var value;
	
	      // there may be a case where accessing cssText throws an error.
	      // I could not reproduce this issue, but the worst that can happen
	      // this way is an animation not running properly.
	      // not awesome, but probably better than a script error
	      // see https://github.com/rodneyrehm/viewport-units-buggyfill/issues/21
	      try {
	        value = rule.cssText;
	      } catch(e) {
	        return;
	      }
	
	      viewportUnitExpression.lastIndex = 0;
	      if (viewportUnitExpression.test(value)) {
	        // KeyframesRule does not have a CSS-PropertyName
	        declarations.push([rule, null, value]);
	        options.hacks && options.hacks.findDeclarations(declarations, rule, null, value);
	      }
	
	      return;
	    }
	
	    if (!rule.style) {
	      if (!rule.cssRules) {
	        return;
	      }
	
	      forEach.call(rule.cssRules, function(_rule) {
	        findDeclarations(_rule);
	      });
	
	      return;
	    }
	
	    forEach.call(rule.style, function(name) {
	      var value = rule.style.getPropertyValue(name);
	      // preserve those !important rules
	      if (rule.style.getPropertyPriority(name)) {
	        value += ' !important';
	      }
	
	      viewportUnitExpression.lastIndex = 0;
	      if (viewportUnitExpression.test(value)) {
	        declarations.push([rule, name, value]);
	        options.hacks && options.hacks.findDeclarations(declarations, rule, name, value);
	      }
	    });
	  }
	
	  function getReplacedViewportUnits() {
	    dimensions = getViewport();
	
	    var css = [];
	    var buffer = [];
	    var open;
	    var close;
	
	    declarations.forEach(function(item) {
	      var _item = overwriteDeclaration.apply(null, item);
	      var _open = _item.selector.length ? (_item.selector.join(' {\n') + ' {\n') : '';
	      var _close = new Array(_item.selector.length + 1).join('\n}');
	
	      if (!_open || _open !== open) {
	        if (buffer.length) {
	          css.push(open + buffer.join('\n') + close);
	          buffer.length = 0;
	        }
	
	        if (_open) {
	          open = _open;
	          close = _close;
	          buffer.push(_item.content);
	        } else {
	          css.push(_item.content);
	          open = null;
	          close = null;
	        }
	
	        return;
	      }
	
	      if (_open && !open) {
	        open = _open;
	        close = _close;
	      }
	
	      buffer.push(_item.content);
	    });
	
	    if (buffer.length) {
	      css.push(open + buffer.join('\n') + close);
	    }
	
	    // Opera Mini messes up on the content hack (it replaces the DOM node's innerHTML with the value).
	    // This fixes it. We test for Opera Mini only since it is the most expensive CSS selector
	    // see https://developer.mozilla.org/en-US/docs/Web/CSS/Universal_selectors
	    if (isOperaMini) {
	      css.push('* { content: normal !important; }');
	    }
	
	    return css.join('\n\n');
	  }
	
	  function overwriteDeclaration(rule, name, value) {
	    var _value;
	    var _selectors = [];
	
	    _value = value.replace(viewportUnitExpression, replaceValues);
	
	    if (options.hacks) {
	      _value = options.hacks.overwriteDeclaration(rule, name, _value);
	    }
	
	    if (name) {
	      // skipping KeyframesRule
	      _selectors.push(rule.selectorText);
	      _value = name + ': ' + _value + ';';
	    }
	
	    var _rule = rule.parentRule;
	    while (_rule) {
	      _selectors.unshift('@media ' + _rule.media.mediaText);
	      _rule = _rule.parentRule;
	    }
	
	    return {
	      selector: _selectors,
	      content: _value
	    };
	  }
	
	  function replaceValues(match, number, unit) {
	    var _base = dimensions[unit];
	    var _number = parseFloat(number) / 100;
	    return (_number * _base) + 'px';
	  }
	
	  function getViewport() {
	    var vh = window.innerHeight;
	    var vw = window.innerWidth;
	
	    return {
	      vh: vh,
	      vw: vw,
	      vmax: Math.max(vw, vh),
	      vmin: Math.min(vw, vh)
	    };
	  }
	
	  function importCrossOriginLinks(next) {
	    var _waiting = 0;
	    var decrease = function() {
	      _waiting--;
	      if (!_waiting) {
	        next();
	      }
	    };
	
	    forEach.call(document.styleSheets, function(sheet) {
	      if (!sheet.href || origin(sheet.href) === origin(location.href) || sheet.ownerNode.getAttribute('data-viewport-units-buggyfill') === 'ignore') {
	        // skip <style> and <link> from same origin or explicitly declared to ignore
	        return;
	      }
	
	      _waiting++;
	      convertLinkToStyle(sheet.ownerNode, decrease);
	    });
	
	    if (!_waiting) {
	      next();
	    }
	  }
	
	  function origin(url) {
	    return url.slice(0, url.indexOf('/', url.indexOf('://') + 3));
	  }
	
	  function convertLinkToStyle(link, next) {
	    getCors(link.href, function() {
	      var style = document.createElement('style');
	      style.media = link.media;
	      style.setAttribute('data-href', link.href);
	      style.textContent = this.responseText;
	      link.parentNode.replaceChild(style, link);
	      next();
	    }, next);
	  }
	
	  function getCors(url, success, error) {
	    var xhr = new XMLHttpRequest();
	    if ('withCredentials' in xhr) {
	      // XHR for Chrome/Firefox/Opera/Safari.
	      xhr.open('GET', url, true);
	    } else if (typeof XDomainRequest !== 'undefined') {
	      // XDomainRequest for IE.
	      xhr = new XDomainRequest();
	      xhr.open('GET', url);
	    } else {
	      throw new Error('cross-domain XHR not supported');
	    }
	
	    xhr.onload = success;
	    xhr.onerror = error;
	    xhr.send();
	    return xhr;
	  }
	
	  return {
	    version: '0.6.0',
	    findProperties: findProperties,
	    getCss: getReplacedViewportUnits,
	    init: initialize,
	    refresh: refresh
	  };
	
	}));


/***/ }

/******/ });
//# sourceMappingURL=preload.js.map